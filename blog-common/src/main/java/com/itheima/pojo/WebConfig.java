package com.itheima.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Table;
import java.util.Date;
@Table(name = "t_web_config")
public class WebConfig {

  private String uid;
  private String logo;
  private String name;
  private String summary;
  private String keyword;
  private String author;
  private String recordNum;
  private String startComment;
  private Long status;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date createTime;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date updateTime;
  private String title;
  private String aliPay;
  private String weixinPay;
  private String github;
  private String gitee;
  private String qqNumber;
  private String qqGroup;
  private String weChat;
  private String email;
  private String showList;
  private String loginTypeList;


  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }


  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }


  public String getKeyword() {
    return keyword;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }


  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }


  public String getRecordNum() {
    return recordNum;
  }

  public void setRecordNum(String recordNum) {
    this.recordNum = recordNum;
  }


  public String getStartComment() {
    return startComment;
  }

  public void setStartComment(String startComment) {
    this.startComment = startComment;
  }


  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }


  public String getAliPay() {
    return aliPay;
  }

  public void setAliPay(String aliPay) {
    this.aliPay = aliPay;
  }


  public String getWeixinPay() {
    return weixinPay;
  }

  public void setWeixinPay(String weixinPay) {
    this.weixinPay = weixinPay;
  }


  public String getGithub() {
    return github;
  }

  public void setGithub(String github) {
    this.github = github;
  }


  public String getGitee() {
    return gitee;
  }

  public void setGitee(String gitee) {
    this.gitee = gitee;
  }


  public String getQqNumber() {
    return qqNumber;
  }

  public void setQqNumber(String qqNumber) {
    this.qqNumber = qqNumber;
  }


  public String getQqGroup() {
    return qqGroup;
  }

  public void setQqGroup(String qqGroup) {
    this.qqGroup = qqGroup;
  }


  public String getWeChat() {
    return weChat;
  }

  public void setWeChat(String weChat) {
    this.weChat = weChat;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  public String getShowList() {
    return showList;
  }

  public void setShowList(String showList) {
    this.showList = showList;
  }


  public String getLoginTypeList() {
    return loginTypeList;
  }

  public void setLoginTypeList(String loginTypeList) {
    this.loginTypeList = loginTypeList;
  }

}
