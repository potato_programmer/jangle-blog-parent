package com.itheima.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Table;
import java.util.Date;
@Table(name = "t_sys_dict_data")
public class SysDictData {

  private String uid;
  private Long oid;
  private String dictTypeUid;
  private String dictLabel;
  private String dictValue;
  private String cssClass;
  private String listClass;
  private Long isDefault;
  private String createByUid;
  private String updateByUid;
  private String remark;
  private Long status;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date createTime;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date updateTime;
  private String isPublish;
  private Long sort;


  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }


  public Long getOid() {
    return oid;
  }

  public void setOid(Long oid) {
    this.oid = oid;
  }


  public String getDictTypeUid() {
    return dictTypeUid;
  }

  public void setDictTypeUid(String dictTypeUid) {
    this.dictTypeUid = dictTypeUid;
  }


  public String getDictLabel() {
    return dictLabel;
  }

  public void setDictLabel(String dictLabel) {
    this.dictLabel = dictLabel;
  }


  public String getDictValue() {
    return dictValue;
  }

  public void setDictValue(String dictValue) {
    this.dictValue = dictValue;
  }


  public String getCssClass() {
    return cssClass;
  }

  public void setCssClass(String cssClass) {
    this.cssClass = cssClass;
  }


  public String getListClass() {
    return listClass;
  }

  public void setListClass(String listClass) {
    this.listClass = listClass;
  }


  public Long getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(Long isDefault) {
    this.isDefault = isDefault;
  }


  public String getCreateByUid() {
    return createByUid;
  }

  public void setCreateByUid(String createByUid) {
    this.createByUid = createByUid;
  }


  public String getUpdateByUid() {
    return updateByUid;
  }

  public void setUpdateByUid(String updateByUid) {
    this.updateByUid = updateByUid;
  }


  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }


  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }


  public String getIsPublish() {
    return isPublish;
  }

  public void setIsPublish(String isPublish) {
    this.isPublish = isPublish;
  }


  public Long getSort() {
    return sort;
  }

  public void setSort(Long sort) {
    this.sort = sort;
  }

}
