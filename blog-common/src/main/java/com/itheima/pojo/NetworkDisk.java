package com.itheima.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Table;
import java.util.Date;
@Table(name = "t_network_disk")
public class NetworkDisk {

  private String uid;
  private String adminUid;
  private String extendName;
  private String fileName;
  private String filePath;
  private Long fileSize;
  private Long isDir;
  private Long status;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date createTime;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date updateTime;
  private String localUrl;
  private String qiNiuUrl;
  private String fileOldName;


  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }


  public String getAdminUid() {
    return adminUid;
  }

  public void setAdminUid(String adminUid) {
    this.adminUid = adminUid;
  }


  public String getExtendName() {
    return extendName;
  }

  public void setExtendName(String extendName) {
    this.extendName = extendName;
  }


  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }


  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }


  public Long getFileSize() {
    return fileSize;
  }

  public void setFileSize(Long fileSize) {
    this.fileSize = fileSize;
  }


  public Long getIsDir() {
    return isDir;
  }

  public void setIsDir(Long isDir) {
    this.isDir = isDir;
  }


  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }


  public String getLocalUrl() {
    return localUrl;
  }

  public void setLocalUrl(String localUrl) {
    this.localUrl = localUrl;
  }


  public String getQiNiuUrl() {
    return qiNiuUrl;
  }

  public void setQiNiuUrl(String qiNiuUrl) {
    this.qiNiuUrl = qiNiuUrl;
  }


  public String getFileOldName() {
    return fileOldName;
  }

  public void setFileOldName(String fileOldName) {
    this.fileOldName = fileOldName;
  }

}
