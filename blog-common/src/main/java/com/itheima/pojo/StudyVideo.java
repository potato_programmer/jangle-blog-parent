package com.itheima.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Table;
import java.util.Date;
@Table(name = "t_study_video")
public class StudyVideo {

  private String uid;
  private String fileUid;
  private String resourceSortUid;
  private String name;
  private String summary;
  private String content;
  private String baiduPath;
  private String clickCount;
  private Long status;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date createTime;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date updateTime;
  private String parentUid;


  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }


  public String getFileUid() {
    return fileUid;
  }

  public void setFileUid(String fileUid) {
    this.fileUid = fileUid;
  }


  public String getResourceSortUid() {
    return resourceSortUid;
  }

  public void setResourceSortUid(String resourceSortUid) {
    this.resourceSortUid = resourceSortUid;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }


  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }


  public String getBaiduPath() {
    return baiduPath;
  }

  public void setBaiduPath(String baiduPath) {
    this.baiduPath = baiduPath;
  }


  public String getClickCount() {
    return clickCount;
  }

  public void setClickCount(String clickCount) {
    this.clickCount = clickCount;
  }


  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }


  public String getParentUid() {
    return parentUid;
  }

  public void setParentUid(String parentUid) {
    this.parentUid = parentUid;
  }

}
