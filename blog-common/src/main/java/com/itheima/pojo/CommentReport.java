package com.itheima.pojo;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import javax.persistence.Table;

@Table(name = "t_comment_report")
public class CommentReport {

  private String uid;
  private String userUid;
  private String reportCommentUid;
  private String reportUserUid;
  private String content;
  private Long progress;
  private Long status;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date createTime;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date updateTime;


  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }


  public String getUserUid() {
    return userUid;
  }

  public void setUserUid(String userUid) {
    this.userUid = userUid;
  }


  public String getReportCommentUid() {
    return reportCommentUid;
  }

  public void setReportCommentUid(String reportCommentUid) {
    this.reportCommentUid = reportCommentUid;
  }


  public String getReportUserUid() {
    return reportUserUid;
  }

  public void setReportUserUid(String reportUserUid) {
    this.reportUserUid = reportUserUid;
  }


  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }


  public Long getProgress() {
    return progress;
  }

  public void setProgress(Long progress) {
    this.progress = progress;
  }


  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
