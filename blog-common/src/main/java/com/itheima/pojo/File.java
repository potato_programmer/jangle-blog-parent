package com.itheima.pojo;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import javax.persistence.Table;

@Table(name = "t_file")
public class File {

  private String uid;
  private String fileOldName;
  private String picName;
  private String picUrl;
  private String picExpandedName;
  private Long fileSize;
  private String fileSortUid;
  private String adminUid;
  private String userUid;
  private Long status;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date createTime;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date updateTime;
  private String qiNiuUrl;


  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }


  public String getFileOldName() {
    return fileOldName;
  }

  public void setFileOldName(String fileOldName) {
    this.fileOldName = fileOldName;
  }


  public String getPicName() {
    return picName;
  }

  public void setPicName(String picName) {
    this.picName = picName;
  }


  public String getPicUrl() {
    return picUrl;
  }

  public void setPicUrl(String picUrl) {
    this.picUrl = picUrl;
  }


  public String getPicExpandedName() {
    return picExpandedName;
  }

  public void setPicExpandedName(String picExpandedName) {
    this.picExpandedName = picExpandedName;
  }


  public Long getFileSize() {
    return fileSize;
  }

  public void setFileSize(Long fileSize) {
    this.fileSize = fileSize;
  }


  public String getFileSortUid() {
    return fileSortUid;
  }

  public void setFileSortUid(String fileSortUid) {
    this.fileSortUid = fileSortUid;
  }


  public String getAdminUid() {
    return adminUid;
  }

  public void setAdminUid(String adminUid) {
    this.adminUid = adminUid;
  }


  public String getUserUid() {
    return userUid;
  }

  public void setUserUid(String userUid) {
    this.userUid = userUid;
  }


  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }


  public String getQiNiuUrl() {
    return qiNiuUrl;
  }

  public void setQiNiuUrl(String qiNiuUrl) {
    this.qiNiuUrl = qiNiuUrl;
  }

}
