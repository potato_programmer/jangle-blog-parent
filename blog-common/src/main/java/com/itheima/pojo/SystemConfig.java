package com.itheima.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Table;
import java.util.Date;
@Table(name = "t_system_config")
public class SystemConfig {

  private String uid;
  private String qiNiuAccessKey;
  private String qiNiuSecretKey;
  private String email;
  private String emailUserName;
  private String emailPassword;
  private String smtpAddress;
  private String smtpPort;
  private Long status;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date createTime;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date updateTime;
  private String qiNiuBucket;
  private String qiNiuArea;
  private String uploadQiNiu;
  private String uploadLocal;
  private String picturePriority;
  private String qiNiuPictureBaseUrl;
  private String localPictureBaseUrl;
  private String startEmailNotification;


  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }


  public String getQiNiuAccessKey() {
    return qiNiuAccessKey;
  }

  public void setQiNiuAccessKey(String qiNiuAccessKey) {
    this.qiNiuAccessKey = qiNiuAccessKey;
  }


  public String getQiNiuSecretKey() {
    return qiNiuSecretKey;
  }

  public void setQiNiuSecretKey(String qiNiuSecretKey) {
    this.qiNiuSecretKey = qiNiuSecretKey;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  public String getEmailUserName() {
    return emailUserName;
  }

  public void setEmailUserName(String emailUserName) {
    this.emailUserName = emailUserName;
  }


  public String getEmailPassword() {
    return emailPassword;
  }

  public void setEmailPassword(String emailPassword) {
    this.emailPassword = emailPassword;
  }


  public String getSmtpAddress() {
    return smtpAddress;
  }

  public void setSmtpAddress(String smtpAddress) {
    this.smtpAddress = smtpAddress;
  }


  public String getSmtpPort() {
    return smtpPort;
  }

  public void setSmtpPort(String smtpPort) {
    this.smtpPort = smtpPort;
  }


  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }


  public String getQiNiuBucket() {
    return qiNiuBucket;
  }

  public void setQiNiuBucket(String qiNiuBucket) {
    this.qiNiuBucket = qiNiuBucket;
  }


  public String getQiNiuArea() {
    return qiNiuArea;
  }

  public void setQiNiuArea(String qiNiuArea) {
    this.qiNiuArea = qiNiuArea;
  }


  public String getUploadQiNiu() {
    return uploadQiNiu;
  }

  public void setUploadQiNiu(String uploadQiNiu) {
    this.uploadQiNiu = uploadQiNiu;
  }


  public String getUploadLocal() {
    return uploadLocal;
  }

  public void setUploadLocal(String uploadLocal) {
    this.uploadLocal = uploadLocal;
  }


  public String getPicturePriority() {
    return picturePriority;
  }

  public void setPicturePriority(String picturePriority) {
    this.picturePriority = picturePriority;
  }


  public String getQiNiuPictureBaseUrl() {
    return qiNiuPictureBaseUrl;
  }

  public void setQiNiuPictureBaseUrl(String qiNiuPictureBaseUrl) {
    this.qiNiuPictureBaseUrl = qiNiuPictureBaseUrl;
  }


  public String getLocalPictureBaseUrl() {
    return localPictureBaseUrl;
  }

  public void setLocalPictureBaseUrl(String localPictureBaseUrl) {
    this.localPictureBaseUrl = localPictureBaseUrl;
  }


  public String getStartEmailNotification() {
    return startEmailNotification;
  }

  public void setStartEmailNotification(String startEmailNotification) {
    this.startEmailNotification = startEmailNotification;
  }

}
