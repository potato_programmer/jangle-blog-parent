package com.itheima.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Table;
import java.util.Date;
@Table(name = "t_storage")
public class Storage {

  private String uid;
  private String adminUid;
  private Long storageSize;
  private Long status;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date createTime;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
  private Date updateTime;


  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }


  public String getAdminUid() {
    return adminUid;
  }

  public void setAdminUid(String adminUid) {
    this.adminUid = adminUid;
  }


  public Long getStorageSize() {
    return storageSize;
  }

  public void setStorageSize(Long storageSize) {
    this.storageSize = storageSize;
  }


  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }


  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }


  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

}
